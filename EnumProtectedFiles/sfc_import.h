#ifndef SFC_IMPORT_H
#define SFC_IMPORT_H

#include <Windows.h>

typedef PVOID SFCHANDLE;

typedef BOOL(__stdcall * BeginFileMapEnumeration)(
    _In_  DWORD Reserved0,    // Always 0
    _In_  PVOID Reserved1,    // Always NULL
    _Out_ SFCHANDLE* Handle   
    );

typedef BOOL(__stdcall * CloseFileMapEnumeration)(
    _In_ SFCHANDLE Handle
    );

typedef struct _PROTECTED_FILE_INFO
{
    DWORD_PTR Length; // in bytes (without \0 symbol on end)
    WCHAR FileName[1];
} PROTECTED_FILE_INFO, *PPROTECTED_FILE_INFO;

typedef BOOL(__stdcall * GetNextFileMapContent)(
    _In_ PVOID Reserved,                     // Always NULL
    _In_ SFCHANDLE SfcHandle,
    _In_ DWORD_PTR Size,
    _Out_ PPROTECTED_FILE_INFO ProtectedInfo,
    _Inout_ PDWORD_PTR dwNeeded
    );

extern BeginFileMapEnumeration  SfcBeginFileMapEnumeration;
extern CloseFileMapEnumeration  SfcCloseFileMapEnumeration;
extern GetNextFileMapContent    SfcGetNextFileMapContent;

BOOL InitSfc(VOID);

#endif // SFC_IMPORT_H