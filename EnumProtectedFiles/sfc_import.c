#include "sfc_import.h"
#include <tchar.h>

#define SFCDLL _T("sfc_os.dll")

BOOL mIsInitialized;
BeginFileMapEnumeration SfcBeginFileMapEnumeration;
CloseFileMapEnumeration SfcCloseFileMapEnumeration;
GetNextFileMapContent   SfcGetNextFileMapContent;

BOOL InitSfc()
{
    HMODULE hModule;

    if (mIsInitialized)
        return TRUE;

    hModule = GetModuleHandle(SFCDLL);
    if (!hModule)
        hModule = LoadLibrary(SFCDLL);

    if (!hModule)
        return FALSE;

    SfcBeginFileMapEnumeration = (BeginFileMapEnumeration)GetProcAddress(hModule, "BeginFileMapEnumeration");
    SfcCloseFileMapEnumeration = (CloseFileMapEnumeration)GetProcAddress(hModule, "CloseFileMapEnumeration");
    SfcGetNextFileMapContent = (GetNextFileMapContent)GetProcAddress(hModule, "GetNextFileMapContent");

    mIsInitialized = NULL != SfcBeginFileMapEnumeration &&
        NULL != SfcCloseFileMapEnumeration &&
        NULL != SfcGetNextFileMapContent;

    return mIsInitialized;
}