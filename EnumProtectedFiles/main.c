#include "sfc_import.h"
#include <wchar.h>

int wmain()
{
    SFCHANDLE SfcHandle = NULL;

    if (!InitSfc())
        return 1;

    if (SfcBeginFileMapEnumeration(0, NULL, &SfcHandle))
    {
        DWORD_PTR            dwBufferSize = 256;
        PPROTECTED_FILE_INFO pData = (PPROTECTED_FILE_INFO)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwBufferSize);

        if (NULL == pData)
        {
            wprintf(L"Can't allocate memory: %u, error %u\n", dwBufferSize, GetLastError());
            SfcCloseFileMapEnumeration(SfcHandle);
            return 2;
        }

        while (TRUE)
        {
            DWORD_PTR dwNeeded = dwBufferSize;
            DWORD     dwLastError = 0;

            SfcGetNextFileMapContent(NULL, SfcHandle, 0, NULL, &dwNeeded);
            dwLastError = GetLastError();

            if (ERROR_NO_MORE_FILES == dwLastError)
                break;

            if (ERROR_INSUFFICIENT_BUFFER != dwLastError)
            {
                wprintf(L"SfcGetNextFileMapContent first call error: %u\n", dwLastError);
                break;
            }

            if (dwNeeded > dwBufferSize)
            {
                while ((dwBufferSize <<= 1) < dwNeeded)
                    ;

                pData = (PPROTECTED_FILE_INFO)HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, pData, dwBufferSize);

                if (NULL == pData)
                {
                    wprintf(L"Can't reallocate memory: %u, error %u\n", dwNeeded, GetLastError());
                    break;
                }
            }

            SetLastError(NO_ERROR);
            SfcGetNextFileMapContent(NULL, SfcHandle, dwBufferSize, pData, &dwNeeded);

            if (NO_ERROR != (dwLastError = GetLastError()))
            {
                wprintf(L"SfcGetNextFileMapContent second call error: %u\n", dwLastError);
                break;
            }

            wprintf(L"%ws\n", pData->FileName);
            //OutputDebugStringW(pData->FileName);
            //OutputDebugStringW(L"\n");
        }

        if (NULL != pData)
        {
            HeapFree(GetProcessHeap(), 0, pData);
            pData = NULL;
        }

        SfcCloseFileMapEnumeration(SfcHandle);
        SfcHandle = NULL;
    }

    wprintf(L"Press any key to end...");
    _getwch();

    return 0;
}